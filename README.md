# Hackerspaces.be website

Git backend for the content of https://hackerspaces.be feel free to fork, and to send us updates. Once we know you're not a spammer, you can get commit rights.

If you just want to add a new space it's easy, look into the `hackerspaces.be/content/spaces` folder, copy one and fill in your details. Editing an exiting space works the same way. The content is formatted with markdown, [tutorial here](http://daringfireball.net/projects/markdown/basics).

This repo gets re-deployed with every commit, so if your pull request has been accepted in the master branch, it will be re-deployed.

You are encouraged to write scripts, for instance, one that would pull a calender feed into git, and displays it nicely on the page. Or a script that checks twitterfeeds for tags and pushes that info to the repo.

## Usage

The website is build with Hugo static site generator
https://gohugo.io/.

To setup a local development env you can use the integrated webserver:

1. Install Hugo https://gohugo.io/getting-started/installing/#quick-install
2. Clone this repo
3. Run `hugo server --disableFastRender` in the root of this repo
4. CConnect to `http://localhost:1313/` with your browser

## Deployment

See `.gitlab-ci.yml`

## Wishlist

* Fancy imageslider on frontpage (every site needs one ;-)
* Logo's of spaces on the page.

## History

This website was originally managed at https://github.com/0x20/hackerspaces.be/ using [Hyde](http://hyde.github.io/).
