---
    title: "2Mades - Makerspace Herentals"
    site: "https://2mades.be/"
    city: "Herentals"
    location: "Moktamee (L01), Bovenrij 30, 2200 Herentals"
    contact: "info@2mades.be"
---

2Mades a coworking and makerspace in Herentals with a [wide range of tools](https://2mades.be/machines/) at their disposal.

Everyone is welcome to work on whatever project they wish, reservation of machines is needed.

* Location: [Bovenrij 30, 2200 Herentals](https://www.openstreetmap.org/node/12045451406)
* Site: https://2mades.be/
