---
    title: "Hackerspace Brussels (HSBXL)"
    site: "https://hsbxl.be/"
    city: "Brussels"
    tel: "003228804004"
    location: "Rue osseghem 53, 1080 Molenbeek Saint-Jean, Brussels"
    contact: "contact@hsbxl.be"
    matrix: #hsbxl:matrix.org
    openday: "Tuesday evening"
    aliases: [spaces/HSBXL.html] #redirect from old site uri
---

Hackerspace Brussels (HSBXL) is a space, dedicated to various aspects of constructive & creative hacking. The space is about 170 square meters, there is an electronics lab with over 9000 components, a library, and lots of tools. You're always welcome to follow one of the workshops or come to the weekly Tuesday meetings, hack nights or other get-together events.

* Location: [Rue osseghem 53, 1080 Molenbeek Saint-Jean, Brussels](https://www.openstreetmap.org/node/7115056369)
* Site: [hsbxl.be](https://hsbxl.be)
* Chat (matrix): [#hsbxl:matrix.org](https://matrix.to/#/#hsbxl:matrix.org)

**Recurring events:**
* [Tech Tuesday](https://hsbxl.be/events/techtuesday/)
* [Open Coworking Day (OCD) every Friday](https://hsbxl.be/events/open-coding-day/)