---
    title: "Voidwarranties"
    site: "https://we.voidwarranties.be/"
    city: "Deurne (Antwerpen)"
    location: "Herrystraat 22, 2100 Deurne (Antwerpen)"
    contact: "@voidwarranties"
    irc: "#voidwarranties on freenode"
    aliases: [spaces/VoidWarranties.html] #redirect from old site uri
---

It is a location where people with common interests, usually in science, technology, digital or electronic art can meet, socialise and collaborate. A hackerspace can be viewed as an open community lab, workbench, machine shop, workshop and/or studio where people of diverse backgrounds can come together to share resources and knowledge to build/make things. Voidwarranties functions as center for peer learning and knowledge sharing, in the form work shops, presentations, and lectures. We also provide space for members to work on their individual projects, or collaborate on group projects with other members

* Location: [Herrystraat 22, 2100 Deurne (Antwerpen)](https://www.openstreetmap.org/node/5580032057)
* Site: [we.voidwarranties.be](https://we.voidwarranties.be/)
