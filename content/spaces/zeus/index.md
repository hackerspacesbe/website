---
    title: "Zeus WPI"
    site: "https://zeus.ugent.be/"
    city: "Ghent"
    location: "Basement S9, campus Sterre, Krijgslaan 281, 9000 Ghent"
    contact: "@ZeusWPI"
    irc: "Replaced with Mattermost (https://mattermost.zeus.gent)"
    aliases: [spaces/Zeus.html] #redirect from old site uri
---

Zeus WPI is the student society of Ghent University that is aimed towards all students who share a common interest in
computer science and technology. We use, contribute and promote free and open source software in all we do. We regularly
organise talks on various topics, ranging from novice to more advanced levels.

We have a hackerspace in the basement of one of the university buildings, S9 on campus _Sterre_. Here, we share our
knowledge and work together on projects that our courses don't cover. Although we're a student society, everyone is free
to come to our events or drop by our little hackerspace.

* Location: [Basement S9, campus Sterre, Krijgslaan 281, 9000 Ghent](https://www.openstreetmap.org/node/5256134433)
* Site: [zeus.ugent.be](https://zeus.ugent.be/)
* Git: [github.com/ZeusWPI](https://github.com/ZeusWPI)