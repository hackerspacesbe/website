---
    title: "UrLab"
    site: "https://urlab.be/"
    city: "Brussels"
    location: "UB2.126 A, 50 Avenue Franklin Roosevelt, 1050 Ixelles"
    contact: "contact@urlab.be"
    irc: "irc.libera.chat#urlab"
    aliases: [spaces/UrLab.html] #redirect from old site uri
---

UrLab is the hackerspace of the [Université Libre de Bruxelles](http://ulb.be). It was founded in the beginning of 2012 by students of many different backgrounds with the idea of bringing the hacker culture to their fellow ULB students.

UrLab is open to everyone, not only to students. We meet every Wednesday evening, feel free to come and hack with us! We put an emphasize on sharing, good atmosphere and openess. [Here is a list of the current projects](https://urlab.be/projects/). We also organize conference sessions called "SmartMonday" each first Monday of the month: they consist of a serie of short talks on technology, hacking, society or anything that sounds cool. The members are mostly French-speaking, but some people are fluent in English; there is no language discrimination, except maybe against PHP (:p).UrLab seems to have a strange obsession with rainbows and poneys, probably because they are awesome!

Location: [UB2.126 A, 50 Avenue Franklin Roosevelt, 1050 Ixelles](https://www.openstreetmap.org/?mlat=50.81171&mlon=4.38301#map=19/50.81171/4.38301)

We are moslty active on IRC -Libera.chat network. Join us on [#urlab](irc://chat.libera.chat/#urlab)

Website : [urlab.be](https://urlab.be)

Mailing list: For that, please register an account on the [incubator](https://urlab.be)

Twitter : [twitter.com/UrLabBxl](https://twitter.com/UrLabBxl)

Youtube : [youtube.com/user/Urlabbxl](https://www.youtube.com/user/Urlabbxl)
