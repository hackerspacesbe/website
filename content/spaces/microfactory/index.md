---
    title: "Micro Factory"
    site: "https://microfactory.be/"
    city: "Molenbeek-Saint-Jean"
    location: "Rue Heyvaert, 140 - 1080 Molenbeek-Saint-Jean"
    contact: "gilles@microfactory.be"
    aliases: [spaces/Microfactory.html] #redirect from old site uri
---

Micro Factory is a manufacturing workshop for professionals and individuals.
It is accessible to everyone by subscription, like a gym club.

It is equipped for working with wood, metal, digital fabrication and many other things shared by its users.

* Location: [Rue Heyvaert, 140 - 1080 Molenbeek-Saint-Jean](https://www.openstreetmap.org/node/10648370727)
* site: [https://microfactory.be/](https://microfactory.be/)
