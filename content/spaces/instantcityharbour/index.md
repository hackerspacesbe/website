---
    title: "🌑 Instant City Harbour"
    site: "http://harbour.instantcity.be/"
    city: "Hennuyeres"
    tel: 0032486608280
    location: "Hennuyères"
    contact: "tom@behets.me"
    irc: #instantcity on Freenode
    aliases: [spaces/InstantCity.html] #redirect from old site uri
    status: "Permanently closed"
---

### Notice: this space is permanently closed

Instant City Harbour is a hackbase, dedicated to nomad living.
The space is about 600 square meters with lots of tools and knowledge.
Come say hi!

* Location: Hennuyères, contact us for specifics.
* Site: [harbour.instantcity.be](https://harbour.instantcity.be)