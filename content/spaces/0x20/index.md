---
    title: "Hackerspace.gent"
    site: "https://www.hackerspace.gent/"
    city: "Ghent"
    location: "Wiedauwkaai 51, 9000 Ghent"
    contact: "info@hackerspace.gent"
    aliases: [spaces/0x20.html] #redirect from old site uri
---

Hackerspace.gent (0x20) is a hackerspace in the wonderful city of Ghent, Belgium. It is a physical space run by a group of people dedicated to various aspects of constructive & creative hacking.
You're always welcome to attend one of the workshops or join the weekly Thursday meetings, the hack nights or the other awesome get-together events. Keep an eye on our wiki (or faecebook) to see whats going on.

* Adress: [Wiedauwkaai 51 Gent](https://www.openstreetmap.org/node/10779879231)
* Site: [hackerspace.gent](https://www.hackerspace.gent/)
