---
    title: "🌑 District Three"
    site: "https://districtthree.be/"
    city: "Lier"
    location: "Kartuizersvest 70, 2500 Lier"
    contact: "info@districtthree.be"
    irc: "http://discord.gg/SfGxJf6"
    aliases: [spaces/DistrictThree.html] #redirect from old site uri
    status: "Permanently Closed"
---

### Notice: this space is permanently closed

District Three is a coworking and makerspace in the center of Lier.

We have a wide variety of machines at our disposal: lasercutter, 3D printers, UV printer, XY plotter, Spot welder, CNC Machines, Sublimation printer, button maker and much more.
A detailed list can be found here: https://districtthree.be/machines.html

Every month there are free coderdojo sessions where children can learn how to program or use all kinds of technology.
More information about these open source workshops can be found here: http://lier.coderdojobelgium.be/
These sessions have been going strong since 2014!

Looking for a new fablab? Visit [2Mades](spaces/2mades) in Herentals.
