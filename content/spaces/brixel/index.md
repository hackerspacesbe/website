---
    title: "Brixel"
    site: "http://www.brixel.be/"
    city: "Hasselt"
    location: "Spalbeekstraat 34, 3510 Spalbeek (Hasselt)"
    contact: "info@brixel.be"
    irc: "replaced with slack (see website for details)"
    aliases: [spaces/Brixel.html] #redirect from old site uri
---


Brixel HQ is located in Spalbeek near Hasselt and we organise events and meetings every Tuesday. Although we do not always have something planned, we try to be open as much as we can to stimulate the magic things that just might happen... Meetings start at around 19:00h localtime, but this can vary a bit. There is no fixed closing time. We welcome everybody in our space. Come have a look, it's fun!

* Location: [Spalbeekstraat 34, 3510 Spalbeek (Hasselt)](https://www.openstreetmap.org/way/752918508)
* Site: [brixel.be](http://www.brixel.be/)