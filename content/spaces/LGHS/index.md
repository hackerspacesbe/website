---
    title: "Liege Hackerspace"
    site: "https://www.lghs.be/"
    city: "Liege"
    location: "Rue de la loi 16, 4020 Liege"
    contact: "ping@lghs.be"
    aliases: [spaces/LGHS.html] #redirect from old site uri
---

Liege Hackerspace is a place where people with an interest for technology, science and art can meet and work together.

We're open every wednesday from 6:30pm to 11pm and every saturday from 1pm to 7pm.

* Location: [Rue de la loi 16, 4020 Liege](https://www.openstreetmap.org/way/222453611)
* Website: [www.lghs.be](https://www.lghs.be)
* Twitter: [@LgHackerSpace](https://twitter.com/LgHackerSpace)
* Facebook: [facebook.com/liegehackerspace](https://www.facebook.com/liegehackerspace/)
* Instagram: [instagram.com/lghackerspace](https://www.instagram.com/lghackerspace/)
