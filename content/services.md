# Mailinglist

Discussion mailinglists for Belgian hackerspaces running on Mailman3. 
- Webinterface: https://discuss.hackerspaces.be


These services follow the values defined by the [Librehosters network](https://libreho.st/).